﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Unit5.Core;
using Xamarin.Forms;
using XamarinUniversity.Infrastructure;
using XamarinUniversity.Interfaces;

namespace Unit5_DataBinding
{
    public class InfoViewModel: INotifyPropertyChanged
    {
        public INavigationService Navigation { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand OnGoBack { get; private set; }

        public InfoViewModel()
        {
        }

        public string ImagePath
        {
            get
            {
                return this._model.ImagePath;
            }
        }

        public bool IsBonusGreaterZero
        {
            get
            {
                return this._model.Bonus > 0;
            }

        }

        public int Bonus
        {
            get
            {
                return this._model.Bonus;
            }
            set
            {
                this._model.Bonus = value;
                this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.Bonus)));
                this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.IsBonusGreaterZero)));
            }
        }

        public string Name
        {
            get { return this._model.Name; }
            set { this._model.Name = value; this.PropertyChanged?.DynamicInvoke(this,new PropertyChangedEventArgs( nameof(this.Name))); }
        }

        public string Email
        {
            get { return this._model.Email; }
            set { this._model.Email = value; this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.Email))); }
        }

        public int GanderIndex
        {
            get { return this.GanderList.IndexOf(c => c.Gander == this._model.Gander); }
            set {
                this._model.Gander = this.GanderList.ElementAt(value).Gander;
                this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.GanderIndex)));
            }
        }

        public IEnumerable<GanderViewModel> GanderList
        {
            get
            {
                return _ganderList ?? new List<GanderViewModel>()
                {
                    new GanderViewModel() { Gander= Gander.Boy, DisplayName="男" },
                     new GanderViewModel() { Gander= Gander.Girl, DisplayName="女" },
                };
            }
        }


        private IEnumerable<GanderViewModel> _ganderList;
        private InfoData _model = new InfoData();

    }
}
